/*
* Template:             Binhats (binhats.com)
* Version:              1.2
* initial release:      -
* Last update:          24.05.2019
* Author:               Ramear Do <rameardone77@gmail.com>
* Contact:              rameardo.com/contact <rameardone77@gmail.com>
* License:              Licensed under Themeforest License
*/


function binhatsDarkTheme() {
    var el1 = document.getElementById("binhatsDarkTheme");
    var el2 = document.getElementById("binhatsLightTheme");
    if(el1.disabled) {
        el1.disabled = false;
        localStorage.setItem("darkreader", "enabled");        

        el2.disabled = true;

    } else {
        el1.disabled = true;
        localStorage.setItem("darkreader", "disabled");
        el2.disabled = false;
    }

}
if (localStorage.getItem("darkreader") == "enabled") {
   document.getElementById("binhatsDarkTheme").disabled = false;
   document.getElementById("binhatsLightTheme").disabled = true;
   
} else {
   document.getElementById("binhatsDarkTheme").disabled = true;
   document.getElementById("binhatsLightTheme").disabled = false;
}
