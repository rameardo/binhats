/*
* Template:             Binhats (binhats.com)
* Version:              1.2
* initial release:      -
* Last update:          24.05.2019
* Author:               Ramear Do <rameardone77@gmail.com>
* Contact:              rameardo.com/contact <rameardone77@gmail.com>
* License:              Licensed under Themeforest License
*/
 
    var options = {
      chart: {
        height: 450,
        type: 'line',
        stacked: false,
        toolbar: {
          show: false,
        },        
      },
      dataLabels: {
        enabled: false
      },
      series: [{
        name: 'Income',
        type: 'column',
        data: [1.4, 2, 2.5, 1.5, 2.5, 2.8, 3.8, 4.6]
      }, {
        name: 'Cashflow',
        type: 'column',
        data: [1.1, 3, 3.1, 4, 4.1, 4.9, 6.5, 8.5]
      }, {
        name: 'Revenue',
        type: 'line',
        data: [20, 29, 37, 36, 44, 45, 50, 58]
      }],
      stroke: {
        width: [1, 1, 4]
      },
      title: {
        text: 'XYZ - Stock Analysis (2009 - 2019)',
        align: 'left',
        offsetX: 65
      },
      xaxis: {
        categories: [2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019],
      },
      yaxis: [
        {
          axisTicks: {
            show: true,
          },
          axisBorder: {
            show: true,
            color: '#1e64e5'
          },
          labels: {
            style: {
              color: '#1e64e5',
            }
          },
          title: {
            text: "Income (thousand crores)",
            style: {
              color: '#1e64e5',
            }
          },
          tooltip: {
            enabled: true
          }
        },

        {
          seriesName: 'Income',
          opposite: true,
          axisTicks: {
            show: true,
          },
          axisBorder: {
            show: true,
            color: '#00E396'
          },
          labels: {
            style: {
              color: '#00E396',
            }
          },
          title: {
            text: "Operating Cashflow (thousand crores)",
            style: {
              color: '#00E396',
            }
          },
        },
        {
          seriesName: 'Revenue',
          opposite: true,
          axisTicks: {
            show: true,
          },
          axisBorder: {
            show: true,
            color: '#FEB019'
          },
          labels: {
            style: {
              color: '#FEB019',
            },
          },
          title: {
            text: "Revenue (thousand crores)",
            style: {
              color: '#FEB019',
            }
          }
        },
      ],
      tooltip: {
        fixed: {
          enabled: true,
          position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
          offsetY: 30,
          offsetX: 60
        },
      },
      legend: {
        position: 'top',
        horizontalAlign: 'left',
        offsetX: 40
      },
      colors: ['#1e64e5', '#00E396', '#FEB019']
    }

    var chart = new ApexCharts(
      document.querySelector("#visitors"),
      options
    );



    chart.render();

  
       

    var eRate = {
      chart: {
        height: 350,
        type: 'line',
        stacked: false,
        toolbar: {
          show: false,
        }
      },
      tooltip: {
        enabled: false,
      },
      stroke: {
        width: [2, 1.5, 1],
        curve: 'straight',
        colors: ['#1e64e5','#dcebf9','#b6cffe'],
      },
      plotOptions: {
        bar: {
          columnWidth: '50%'
        }
      },
      colors: ['#1e64e5', '#dcebf9', '#dce6ec'],
      series: [{
        name: 'EUR-USD',
        type: 'area',
        data: [23, 11, 22, 27, 13, 22, 37, 21, 44, 22, 30, 27, 13, 22, 37, 21]
      }, {
        name: 'GBP-USD',
        type: 'bar',
        data: [44, 55, 41, 67, 22, 43, 21, 41, 56, 27, 43, 67, 22, 43, 21, 41]
      }, {
        name: 'NZD-USD',
        type: 'area',
        data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39, 30, 45, 35, 64, 52]
      }],
      fill: {
        opacity: [0.04,0.5,0.5],
        type: ['solid', 'gradient', 'gradient'],
                gradient: {
                    inverseColors: false,
                    shade: 'dark',
                    type: "horizontal",
                    inverseColors: false,
                    shadeIntensity: 0,
                    opacityFrom: 0,
                    opacityTo: 0,
                    stops: [0, 100, 100, 100]
                }
      },
      labels: ['00:00','01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00'],
      markers: {
        size: 0
      },
      zoom: {
        enabled: false,
      },
      legend: {
        show:false,
      },
      grid: {
        borderColor: '#dddddd',
        position: 'front',
        xaxis: {
          lines: {
            show: true,
          },
          row: {
            colors: ['#e5e5e5', 'red'],
            opacity: 0.5
          }, 
          column: {
              colors: ['#f8f8f8', 'red'],
          }, 
        }
      },
      xaxis: {
            axisBorder: {
              show: false,
            },
            axisTicks: {
              show: false,
            }        
      }
    }

    var eRateChart = new ApexCharts(
      document.querySelector("#exchangeRatess"),
      eRate
    );

    eRateChart.render();

  









    window.Apex = {
      stroke: {
        width: 4
      },
      markers: {
        size: 2
      },
      tooltip: {
        fixed: {
          enabled: true,
        }
      }
    };
    
    var randomizeArray = function (arg) {
      var array = arg.slice();
      var currentIndex = array.length,
        temporaryValue, randomIndex;

      while (0 !== currentIndex) {

        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }

      return array;
    }

    // data for the sparklines that appear below header area
    var sparklineData = [47, 45, 54, 38, 56, 24, 65, 31, 37, 39, 62, 51, 35, 41, 35, 27, 93, 53, 61, 27, 54, 43, 19, 46];



    var spark2 = {
      chart: {
        type: 'area',
        height: 260,
        sparkline: {
          enabled: true
        },
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        opacity: 0.9,
      },
      series: [{
        data: randomizeArray(sparklineData)
      }],
      yaxis: {
        min: 0
      },
      colors: ['#DCE6EC'],
      title: {
        text: '$235,312',
        offsetX: 0,
        style: {
          fontSize: '24px',
          color: '#FFF',
          cssClass: 'apexcharts-yaxis-title'
        }
      },
      subtitle: {
        text: 'Total Earning',
        offsetX: 0,
        style: {
          fontSize: '14px',
          color: '#FFF',
          cssClass: 'apexcharts-yaxis-title'
        }
      }
    }


  
    var spark2 = new ApexCharts(document.querySelector("#spark2"), spark2);
    
    spark2.render();
   
   



    window.Apex = {
      stroke: {
        width: 2
      },
      markers: {
        size: 0
      },
      tooltip: {
        fixed: {
          enabled: true,
        }
      }
    };   

    var sparklineData = [0, 0, 0, 0, 45, 0, 0, 0, 0, 39, 0, 0, 0, 0, 0, 27, 0, 53, 0, 27, 0, 43, 0, 46];

    var smallTraffic1 = {
      chart: {
        type: 'area',
        height: 20,
        width: '100%',
        sparkline: {
          enabled: true
        },
      },
      fill: {
        colors: ['#1A73E8', '#B32824']
      },
      tooltip: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        opacity: 0,
      },
      series: [{
        data: randomizeArray(sparklineData)
      }],

      colors: ['#DCE6EC'],
    }


    var smallTraffic1 = new ApexCharts(document.querySelector("#smallTraffic1"), smallTraffic1);
    smallTraffic1.render();

    // var sparklineData = [0, 0, 1.2010, 1.1101, 0, 1.1521, 0, 1.8241, 0, 1.17166, 1.3533, 0, 0, 1.521, 0, 1.19445 , 0, 1.1921, 0, 1.1115, 0, 1.1621, 0, 1.2110];
    var sparklineData = [0, 0, 0, 0, 1.2010, 0, 0, 0, 0, 1.1101, 0, 0, 0, 0, 0, 1.1716, 0, 1.8241, 0, 1.1621, 0, 1.1921, 0, 1.2110];
    var exchangeChart = {
      chart: {
        type: 'area',
        height: 50,
        width: '100%',
        sparkline: {
          enabled: true
        },
      },
      fill: {
        colors: ['#1A73E8', '#B32824']
      },
      tooltip: {
        enabled: true
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        opacity: 0,
      },
      series: [{
        name: 'EUR-USD',
        data: randomizeArray(sparklineData)
      }],

      colors: ['#DCE6EC'],
    }


    var exchangeChart = new ApexCharts(document.querySelector("#exchange-chart"), exchangeChart);
    exchangeChart.render();

    var sparklineData = [0, 0, 0, 0, 45, 0, 0, 0, 0, 39, 0, 0, 0, 0, 0, 27, 0, 53, 0, 27, 0, 43, 0, 46];
    var smallTraffic2 = {
      chart: {
        type: 'area',
        height: 20,
        width: '100%',
        sparkline: {
          enabled: true
        },
      },
      tooltip: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        opacity: 0,
      },
      series: [{
        data: randomizeArray(sparklineData)
      }],


      colors: ['#DCE6EC'],

    }


    var smallTraffic2 = new ApexCharts(document.querySelector("#smallTraffic2"), smallTraffic2);
    smallTraffic2.render();


    var smallTraffic3 = {
      chart: {
        type: 'area',
        height: 20,
        width: '100%',
        sparkline: {
          enabled: true
        },
      },
      tooltip: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        opacity: 0,
      },
      series: [{
        data: randomizeArray(sparklineData)
      }],

      colors: ['#DCE6EC'],

    }


    var smallTraffic3 = new ApexCharts(document.querySelector("#smallTraffic3"), smallTraffic3);
    smallTraffic3.render();



    var smallTraffic4 = {
      chart: {
        type: 'area',
        height: 20,
        width: '100%',
        sparkline: {
          enabled: true
        },
      },
      tooltip: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        opacity: 0,
      },
      series: [{
        data: randomizeArray(sparklineData)
      }],

      colors: ['#DCE6EC'],

    }


    var smallTraffic4 = new ApexCharts(document.querySelector("#smallTraffic4"), smallTraffic4);
    smallTraffic4.render();


    var cryptoBtc = {
      chart: {
        type: 'bar',
        width: '100%',
        height: 35,
        sparkline: {
          enabled: true
        }

      },
      tooltip: {
        enabled: false
      },
      plotOptions: {
        bar: {
          columnWidth: '1%',
          fill : 'red',
        },
      },
      series: [{
        data: [25, 66, 41, 89, 63, 65, 44, 72, 36, 59, 54, 25, 66, 41, 89, 63,  41, 89, 63, 25, 44, 12, 36, 9, 54 ]
      }],
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],

      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      colors: ['#DCE6EC']

    }

    var cryptoXrp = {
      chart: {
        type: 'bar',
        width: '100%',
        height: 35,
        foreColor: '#999',
        sparkline: {
          enabled: true
        }

      },
      tooltip: {
        enabled: false
      },
      plotOptions: {
        bar: {
          columnWidth: '1%',

        }
      },
      series: [{
        data: [12, 8, 55, 34, 86, 33, 14, 25, 56, 9, 54, 25, 66, 41, 89, 63,  41, 89, 63, 25, 44, 12, 36, 9, 54 ]
      }],
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],

      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      colors: ['#DCE6EC']

    }

    var cryptoEth = {
      chart: {
        type: 'bar',
        width: '100%',
        height: 35,
        sparkline: {
          enabled: true
        }

      },
      tooltip: {
        enabled: false
      },
      plotOptions: {
        bar: {
          columnWidth: '1%'
        }
      },
      series: [{
        data: [25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54, 25, 66, 41, 89, 63,  41, 89, 63, 25, 44, 12, 36, 9, 54 ]
      }],
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],

      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      colors: ['#DCE6EC']

    }

    var cryptoIota = {
      chart: {
        type: 'bar',
        width: '100%',
        height: 35,

        sparkline: {
          enabled: true
        }

      },
      tooltip: {
        enabled: false
      },
      plotOptions: {
        bar: {
          columnWidth: '1%',
          color: 'red'
        }
      },
      series: [{
        data: [25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54, 25, 66, 41, 89, 63,  41, 89, 63, 25, 44, 12, 36, 9, 54 ]
      }],
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],

      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      colors: ['#DCE6EC']

    }    


    var netRequest = {
      chart: {
        type: 'bar',
        width: '100%',
        height: 50,

        sparkline: {
          enabled: true
        }

      },
      tooltip: {
        enabled: false
      },
      plotOptions: {
        bar: {
          columnWidth: '1%',
          color: 'red'
        }
      },
      series: [{
        data: [25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54, 25, 66, 41, 89, 63,  41, 89, 63, 25, 44, 12, 36, 9, 54 ]
      }],
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],

      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      colors: ['#1e64e5']

    }

    var netResponse = {
      chart: {
        type: 'bar',
        width: '100%',
        height: 50,

        sparkline: {
          enabled: true
        }

      },
      tooltip: {
        enabled: false
      },
      plotOptions: {
        bar: {
          columnWidth: '1%',
          color: 'red'
        }
      },
      series: [{
        data: [25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54, 25, 66, 41, 89, 63,  41, 89, 63, 25, 44, 12, 36, 9, 54 ]
      }],
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],

      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      colors: ['#23b397']

    }


    new ApexCharts(document.querySelector("#crypto-xrp"), cryptoXrp).render();
    new ApexCharts(document.querySelector("#crypto-btc"), cryptoBtc).render();
    new ApexCharts(document.querySelector("#crypto-eth"), cryptoEth).render();
    new ApexCharts(document.querySelector("#crypto-iota"), cryptoIota).render();
    new ApexCharts(document.querySelector("#netRequest"), netRequest).render();
    new ApexCharts(document.querySelector("#netResponse"), netResponse).render();

  

        var options = {
            chart: {
                height: 240,
                
                type: 'bar',
             
                toolbar: {
                  show: false,
                },
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '50%',
                    endingShape: 'rounded',
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            series: [
            {
                name: 'Net Profit',
                data: [44, 55, 57, 56, 61, 58, 63, 60, 66]

            }, {
                name: 'Revenue',
                data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
            }],
            xaxis: {
                categories: ['May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct'],

                axisBorder: {
                    show: false,
                },  
                axisTicks: {
                    show: false,
                },                
            },
            fill: {
              colors: ['#DCE6EC', '#1e64e5'],
              opacity: 1

            },
            legend: {
              show: false,
            },
            tooltip: {
                enabled: false,
            },
            grid: {
              show: true,
              borderColor: '#f5f5f5',
          }

        }

        var chart = new ApexCharts(
            document.querySelector("#stock-value"),
            options
        );

        chart.render();
       
  

        var options = {
            chart: {
                height: 250,
                width: '100%',
                type: 'bar',
             
                toolbar: {
                  show: false,
                },
            },
            plotOptions: {
                bar: {
                    horizontal: true,
                    columnWidth: '50%',
                    endingShape: 'rounded',
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            series: [
            {
                name: 'Net Profit',
                data: [44, 55, 57, 56, 61, 58, 63, 60, 66]

            }, {
                name: 'Revenue',
                data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
            }],
            xaxis: {
                categories: ['May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct'],

                axisBorder: {
                    show: false,
                },  
                axisTicks: {
                    show: false,
                },                
            },
            fill: {
              colors: ['#DCE6EC', '#1e64e5'],
              opacity: 1

            },
            legend: {
              show: false,
            },
            tooltip: {
                enabled: false,
            },
            grid: {
              show: true,
              borderColor: '#f5f5f5',
          }

        }

        var chart = new ApexCharts(
            document.querySelector("#stock-values"),
            options
        );

        chart.render();
       









    
    var options = {
      chart: {
        height: 350,
        type: 'area',
        stacked: true,
        events: {
          selection: function(chart, e) {
            console.log(new Date(e.xaxis.min) )
          },
        },

        toolbar: {
          show: false,
        },

      },
      colors: ['#ccc'],
      dataLabels: {
          enabled: false
      },
      stroke: {
        curve: 'straight',
        colors: ['#1e64e5', '#749ae0', '#98a6ad'],
        width: [0.2,0.5,1]
      },

      series: [{
          name: 'TCP',
          data: generateDayWiseTimeSeries(new Date('11 Feb 2017 GMT').getTime(), 60, {
            min: 1,
            max: 10
          })
        },
        {
          name: 'UDP',
          data: generateDayWiseTimeSeries(new Date('11 Feb 2017 GMT').getTime(), 60, {
            min: 5,
            max: 20
          })
        },
        
        {
          name: 'HTTP',
          data: generateDayWiseTimeSeries(new Date('11 Feb 2017 GMT').getTime(), 60, {
            min: 10,
            max: 40
          })
        }
      ],
      fill: {
        colors: ['#98a6ad', '#749ae0', '#1e64e5'],
        
        opacity: [0.01, 0, 0 ],
        type: 'solid',
        // gradient: {
        //   opacityFrom: [0,0,0],
        //   opacityTo: [0.3, 0.3, 0],
        // }
      },
      legend: {
        show: false,
      },
      xaxis: {
        type: 'datetime',
        labels: {
          show: false,
        },
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
      },
    }

    var chart = new ApexCharts(
      document.querySelector("#network-traffic"),
      options
    );

    chart.render();

    /*
      // this function will generate output in this format
      // data = [
          [timestamp, 23],
          [timestamp, 33],
          [timestamp, 12]
          ...
      ]
      */
    function generateDayWiseTimeSeries(baseval, count, yrange) {
      var i = 0;
      var series = [];
      while (i < count) {
        var x = baseval;
        var y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

        series.push([x, y]);
        baseval += 86400000;
        i++;
      }
      return series;
    }









var optionsBar = {
  chart: {
    type: 'bar',
    height: 250,
    width: '100%',
    stacked: true
  },
  tooltip: {
    enabled: false,
  },
  dataLabels: {
    enabled: false,
  },
  plotOptions: {
    bar: {
      dataLabels: {
        enabled: false
      },
      columnWidth: '75%',
      endingShape: 'rounded'
    }
  },

  colors: ["#2de967", '#F3F2FC'],
  series: [{
    name: "Sessions",
    data: [20, 16, 24, 28, 26, 22, 15, 5, 14, 16, 22, 29, 24, 19, 15, 10, 11, 15, 19, 23],
  }, {
    name: "Views",
    data: [20, 16, 24, 28, 26, 22, 15, 5, 14, 16, 22, 29, 24, 19, 15, 10, 11, 15, 19, 23],
  }],
  labels: [15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 1, 2, 3, 4],
  xaxis: {
    axisBorder: {
      show: false
    },
    axisTicks: {
      show: false
    },
    crosshairs: {
      show: false
    },
    labels: {
      show: false,
      style: {
        fontSize: '14px'
      }
    },
  },
  grid: {
    xaxis: {
      lines: {
        show: false
      },
    },
    yaxis: {
      lines: {
        show: false
      },
    }
  },
  yaxis: {
    axisBorder: {
      show: false
    },
    labels: {
      show: false
    },
  },
  legend: {
    floating: true,
    position: 'top',
    horizontalAlign: 'right',
    offsetY: -36
  },
  title: {
    text: 'Web Statistics',
    align: 'left',
  },
  subtitle: {
    text: 'Sessions and Views'
  },


}

var chartBar = new ApexCharts(document.querySelector('#bar'), optionsBar);
chartBar.render();

var optionsCircle1 = {
  chart: {
    type: 'radialBar',
    height: 250,
    zoom: {
      enabled: false
    },
  },
  colors: ['#E91E63'],
  plotOptions: {
    radialBar: {
      dataLabels: {
        name: {
          show: false
        },
        value: {
          offsetY: 0
        }
      }
    }
  },
  series: [65],
  theme: {
    monochrome: {
      enabled: false
    }
  },
  title: {
    text: 'Bounce Rate',
    align: 'left'
  }
}












        var options = {
            chart: {
                height: 250,
                type: 'radialBar',
            },
            plotOptions: {
                radialBar: {
                    hollow: {

                        size: '80%',
                    }
                },
            },
            series: [80],
            labels: ['REACHED'],

        }

        var chart = new ApexCharts(
            document.querySelector("#search"),
            options
        );

        chart.render();











        var options = {
            chart: {
                height: 150,
                type: 'bar',
                width: '80%',
                toolbar: {
                  show: false,
                },
                tooltip: {
                  show: false,
                },
                
            },
            tooltip: {
                  enabled: false,
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '15%',
                    endingShape: 'rounded'  
                },
                
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            series: [{

                data: [44, 55, 57, 56, 61, 58]
            }],
            xaxis: {
                categories: ['May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct'],
                 axisBorder: {
                  show: false
                 },
                 axisTicks: {
                  show: false,
                 },
                 labels: {
                  show: true,
                  style: {
                    colors: '#FFF'
                  }
                 }
            },
            yaxis: {
              show: false,
            },
            fill: {
                opacity: 1
            },
            
            grid: {
              borderColor: 'transparent'
            },
            colors: ['#c1d6ff']
        }

        var chart = new ApexCharts(
            document.querySelector("#FD-bar"),
            options
        );

        chart.render();

       var chart1 = new ApexCharts(
            document.querySelector("#FD-bar1"),
            options
        );

        chart1.render();
       
          