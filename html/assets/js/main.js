/*
* Template:             Binhats (binhats.com)
* Version:              1.2
* initial release:      -
* Last update:          24.05.2019
* Author:               Ramear Do <rameardone77@gmail.com>
* Contact:              rameardo.com/contact <rameardone77@gmail.com>
* License:              Licensed under Themeforest License
*/


[].slice.call(document.querySelectorAll('.dropdown .nav-link')).forEach(function(el){
    el.addEventListener('click', onClick, false);
});

function onClick(e){
    e.preventDefault();
    var el = this.parentNode;
    el.classList.contains('show-submenu') ? hideSubMenu(el) : showSubMenu(el);
}

function showSubMenu(el){
    el.classList.add('show-submenu');
    document.addEventListener('click', function onDocClick(e){
        e.preventDefault();
        if(el.contains(e.target)){
            return;
        }
        document.removeEventListener('click', onDocClick);
        hideSubMenu(el);
    });
}

function hideSubMenu(el){
    el.classList.remove('show-submenu');
}



$(function() {
  var Accordion = function(el, multiple) {
    this.el = el || {};
    
    this.multiple = multiple || false;
    
    var dropdownlink = this.el.find('.dropdownlink');
    dropdownlink.on('click',
                    { el: this.el, multiple: this.multiple },
                    this.dropdown);
  };
  
  Accordion.prototype.dropdown = function(e) {
    var $el = e.data.el,
        $this = $(this),
        
        $next = $this.next();
    
    $next.slideToggle();
    $this.parent().toggleClass('open');
    
    if(!e.data.multiple) {
      
      $el.find('.submenuItems').not($next).slideUp().parent().removeClass('open');
    }
  }
  
  var accordion = new Accordion($('.accordion-menu'), false);
})

$(window).on('load', function() {
  $('.one').fadeOut();
  $('.loader').fadeOut('slow');
  $('.container').css({'display':'block'});
});
// var elem = document.querySelector('.checkbox');
// // var init = new Switchery(elem);
// var switchery = new Switchery(elem, { size: 'small', color : '#1e64e5'  });

// var elem = document.querySelector('.checkbox');
// var init = new Switchery(elem);

var elems = Array.prototype.slice.call(document.querySelectorAll('.checkbox'));

elems.forEach(function(html) {
  var switchery = new Switchery(html, { size: 'small', color : '#1e64e5'  });
});


var leftMenuBar = document.getElementById('sidebarLeft'),
		rightMenu = document.getElementById('sidebarRight'),
    searchbar = document.getElementById('searchbar'),
		layout = document.getElementsByClassName("container"),
    FullLayout = document.getElementById("Full");





// Button Loader
$(function() {        
  $('.btn-loader').click(function() {
    var $this = $(this);

    var originalText = $this.text();

    $(this).toggleClass("button--loading");

    $this.text('Saving..');

    setTimeout(function() {

      $this.text('Saved');

      $('.form-btn').removeClass("button--loading");
      $('.btn').removeClass("button--loading");

      $('.btn-loader').toggleClass('success-btn-loading');
      

      setTimeout(function(){  

        $('.btn-loader').removeClass('success-btn-loading');
        // $('.btn-loader').toggleClass('animated jello'); Add Animation

        $this.text(originalText);

      }, 1000);

    }, 2000); 

  });


});


// Alerts Close
$(function(){
  $('.alert-close').click(function(){
    if ($('.alerts').hasClass('close')) {
      $('.alerts.close').toggleClass('animated flipOutX');
      setTimeout(function(){
        $('.alerts.close').css('display', 'none');
      }, 500)
    }
  });
});

// Show Hide Code 
// $('.content-border pre').hide();

// $('.b-ui-content-title').click(function(e){
    
//     e.preventDefault();
//     // hide all span
//     var $this = $(this).parent().find('pre');
//     $(".content-border pre").not($this).hide();
    
//     // here is what I want to do
//     $this.toggle();
    
// });



function leftMenu()
{
	leftMenuBar.style.left = '0px';
}

function closeLeftMenu(){
  leftMenuBar.style.left = '-400%';
}

document.addEventListener("DOMContentLoaded", function(event) {
  new MetisMenu('#leftmenu', {
    toggle: true

  });
});

document.addEventListener("DOMContentLoaded", function(event) {
  new MetisMenu('#settingsMenu', {
    toggle: true
  });
});


function phoneSearchBar()
{
	searchbar.style.top = '4px';
}
function openRightMenu() 
{
	rightMenu.style.right = '0';
	rightMenu.style.display = 'unset';
}
function closeRightMenu()
{
	rightMenu.style.right = '-100%';
}
function fullLayout()
{
	layout[0].style.width = '100%';
	layout[1].style.width = '100%';
	layout[2].style.width = '100%';
	layout[3].style.width = '100%';
	layout[4].style.width = '100%';
	layout[5].style.width = '100%';
	layout[6].style.width = '100%';
	FullLayout.style.border = 'red';
	rightMenu.style.right = '-100% !important';
}
function standartLayout()
{
	layout[0].style.width = '1200px';
	layout[1].style.width = '1200px';
	layout[2].style.width = '1200px';
	layout[3].style.width = '1200px';
	layout[4].style.width = '1200px';
	layout[5].style.width = '1200px';
	layout[6].style.width = '1200px';
}



// Cookies Message
if (!localStorage.getItem("cookiesAccepted")) { 
  var cookieMessage = document.getElementById('cookie-notification');  
  var closeCookie = document.getElementById('cookie-notification-close');
  
  setTimeout(function(){
    $(document).ready(function () {
        $("#cookie-notification").fancybox({
            'overlayShow': true,
            toolbar  : false,
        }).trigger('click');
    });
  }, 3000);

  closeCookie.addEventListener("click", function(e) {  
    e.preventDefault();
    localStorage.setItem("cookiesAccepted", true);
    
    cookieMessage.style.display = 'none';
  });
} 



function toPath(path){window.location = path;}




/**
* Generate Random Price between range called by attributes
* @attribute 'binhats-min-price' = set minmum price for currency or some number
* @attribute 'binhats-max-price' = set maximum price for currency or some number
* @attribute 'binhats-price-fixed' = set how many numbers show some comma
* @attribute 'binhats-price-duration' = set time duration for change numbers between range
* Syntax: <span class="realTimePrice" binhats-min-price="int" binhats-max-price="int"  binhats-price-fixed="int">Error Handler</span>
* Example"Bitcoin Price": <span class="realTimePrice" binhats-min-price="8006" binhats-max-price="8066" binhats-price-fixed="4">Something wrong with fetch the price</span>
* Working Basis:
*  if price less than avarage price, add color red [called by css class, in this case is class 'negative' => red]
*  else price more than avarage price, add green color [called by css class, in this case is class 'positive' => green]
*/
function realTimePrice(){
  $('.realTimePrice').each(function(){
    let minPrice = $(this).attr('binhats-min-price')
    let maxPrice = $(this).attr('binhats-max-price')
    let priceFix = $(this).attr('binhats-price-fixed')
    let priceDuration = $(this).attr('binhats-price-duration')
    let priceAvarage = (maxPrice - minPrice) / 2

    let realTimePrice = (Math.random() * (Number(maxPrice) - Number(minPrice)) + Number(minPrice)).toFixed(priceFix)

    if (priceFix < 1){
      realTimePrice = (Math.random() * (Number(maxPrice) - Number(minPrice)) + Number(minPrice)).toFixed(2)
    }

    if (realTimePrice < maxPrice - priceAvarage) {
      $(this).addClass('negative')
    }
    else{
      $(this).removeClass('negative')
      $(this).addClass('positive')
    }

    $(this).text(realTimePrice)

  })

  setTimeout(realTimePrice, 1500)
}


/**
* Add to element a spiner with dynamicly changing data called by attributes
* @attribute 'binhats-button-before' set Before text value that's will shown after clicked the button or element
* @attribute 'binhats-button-after' set after text value thatÄs will shown after @attribute 'binhats-button-before'
* @attribute 'binhats-button-loader' this attribute has 2 propierties 'on' & 'off', if you define attribute as on it will be added loader or 'spiner' to button or element
* syntax: <button class='BH-btn-loader' binhats-button-before='string value or something else' binhats-button-after='string value or something else' binhats-button-loader='on OR off'>button value </button>
* example: <button class='BH-btn-loader' binhats-button-before='Creating an account...' binhats-button-after='Redirect...' binhats-button-loader='on'>Register Now </button>
*/
function binhatsButtonLoader(){
  $(function(){

    $('.BH-btn-loader').click(function(){

      let $this = $(this)

      $this.click(false)

      let binhatsButtonValue = $this.text()

      let binhatsButtonBefore = $this.attr('binhats-button-before')
      let binhatsButtonAfter = $this.attr('binhats-button-after')
      let binhatsButtonLoader = $this.attr('binhats-button-loader')

      if (binhatsButtonLoader == 'on'){
        $this.addClass('binhats-button-loader')
      }
      
      $this.css('opacity', '.5')

      $this.text(binhatsButtonBefore)

      setTimeout(function(){

        $this.css('opacity', '.8')
        $this.text(binhatsButtonAfter)

        setTimeout(function(){

          $this.css('opacity', '1')
          $this.text(binhatsButtonValue)
          $this.removeClass('binhats-button-loader')

        },1000)

      },2500)
      

    })

  })

}