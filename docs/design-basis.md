##### Author

- Ramear Do

### Colors

Base-color: 3751ff, 1e64e5

icon color: eceff1, 1e64e5

font-title : 414d5f, 525c5d
font-desc : 98a6ad


border: dfdfdf, c0ccda
border-hover: adadad, 8392a5

Flex-Gradient
Green: 3cce69 - 2de967


basic-table th: 8392a5
basic-table td: 001737

paragraph : 1b2e4b
paragraph : 8392a5

progress-background : e3e7ed

hover: e7eaef, d9dce0, b1b1b1





### Dark color

Base color: 

a hover: 0b121f
left menu : 0e1627