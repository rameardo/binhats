<!-- ![hgpEYXY.jpg](https://i.imgur.com/WWvfbzh.jpg) -->

<!-- ![zHff9uL.png](https://i.imgur.com/zHff9uL.png) -->

![568uctv.png](https://i.imgur.com/568uctv.png)

<a href="https://rameardo.gitlab.io/binhats/html/finance-dashboard" taget="__blank"><img src="https://i.imgur.com/o59rtoT.png" width="150px"> </a>

Binhats is admin dashboard, It is fully responsive and included awesome features to help build web applications fast and easy. The tones of well designed and developed layouts, components, elements, widgets and pages allows you to create any based interface, custom admin panel or dashboard application. The clean and flexible code enable you to easily customize it.



##### FEATURES

- Dashboards
    - Finance Dashboard
    - Crypto Dashboard
    - Analytics Dashboard
    - Statistic Dashboard
    - eCommerce Dashboard
- Apps
    - E-Mail App
    - Chat App
    - Todo App
    - Calendar App
    - Customer Support (CRM System)
    - Project-manager App
    - Filemanager App (Grid & List)
    - Contacts App (Grid & List)
- Basic UI Components
    - Alerts
    - Badges
    - Buttons
    - Pagination
    - Dropdown
    - Cards
    - Colors
    - Carousel (Sliders)
    - Typography
    - Modals
    - Tabs
    - Tooltip
    - Icons 
- Tables
    - Basic Tables
    - Data Tables
    - Advanced Tables
- Forms
    - Basis Form
    - Advanced Form
    - Form Validation
    - Form Wizard
    - File Upload
- Maps
    - Google Maps
    - Vector Maps
- Charts
    - Apex Charts
- Pages
    - Recovery Password
    - E-Mail Templates
    - Search Result
    - Pricing Table
    - Coming soon
    - Maintenance
    - Lock Screen
    - Blank page (HTML-Strcture included left-menu & header to build new page)
    - Timeline
    - Register
    - Invoice
    - Profile
    - Login
    - 404 Page
    - 500 Page
    - 503 Page
- Responsive with
    - Laptops & Displays
    - Tablets 
    - Mobile
- Layouts
    - Horizontal
    - Vertical
- Themes
    - Light Theme
    - Dark Theme
- Clean and Flat design
- HTML5, CSS3 and Javascript
- <a href="https://fonts.google.com/specimen/Roboto">Roboto</a> Fonts

##### Compatible Browsers	

- Internet-Explorer [IE10, IE11]
- Edge
- Firefox
- Safari
- Opera
- Chrome


##### Credits (current allready used)

| Name | Reference |
| --- | --- |
| Jquery | [https://github.com/jquery/jquery](https://github.com/jquery/jquery) |
| Switchery | [https://github.com/abpetkov/switchery](https://github.com/abpetkov/switchery) |
| Apex chart | [https://github.com/apexcharts/apexcharts.js](https://github.com/apexcharts/apexcharts.js) |
| Animate.css| [https://github.com/daneden/animate.css](https://github.com/daneden/animate.css) |
| Boxicons| [https://github.com/atisawd/boxicons/](https://github.com/atisawd/boxicons/) |
| flaticon| [https://www.flaticon.com/](https://www.flaticon.com/) |
| Metismenujs| [https://github.com/onokumus/metismenujs](https://github.com/onokumus/metismenujs) |
| tippyjs| [https://github.com/atomiks/tippyjs](https://github.com/atomiks/tippyjs) |
| sortablejs| [https://github.com/SortableJS/Sortable](https://github.com/SortableJS/Sortable) |
| fancybox| [https://github.com/fancyapps/fancybox](https://github.com/fancyapps/fancybox) |
| OwlCarousel2| [https://github.com/OwlCarousel2/OwlCarousel2](https://github.com/OwlCarousel2/OwlCarousel2) |
| air-datepicker| [https://github.com/t1m0n/air-datepicker](https://github.com/t1m0n/air-datepicker) |
| selectizejs| [https://github.com/selectize/selectize.js](https://github.com/selectize/selectize.js) |
| pretty-checkbox| [https://github.com/lokesh-coder/pretty-checkbox](https://github.com/lokesh-coder/pretty-checkbox) |
| File-tree| [https://github.com/GraphicDesignElite/file-tree](https://github.com/GraphicDesignElite/file-tree) |
| jQuery-contextMenu| [https://github.com/swisnl/jQuery-contextMenu](https://github.com/swisnl/jQuery-contextMenu) |
| Prism| [https://github.com/PrismJS/prism](https://github.com/PrismJS/prism) |
| jQuery Marquee | [https://github.com/aamirafridi/jQuery.Marquee](https://github.com/aamirafridi/jQuery.Marquee) |
| jvectormap | [https://github.com/bjornd/jvectormap](https://github.com/bjornd/jvectormap) |
<!-- | jQuery Easy Ticker | [https://github.com/vaakash/jquery-easy-ticker](https://github.com/vaakash/jquery-easy-ticker) | -->



##### Contributing

1. Create a branch from the `dev` branch
2. Implement your new feature
3. Submit a pull request to be merge in the `dev` branch

##### Contributors

- <a href="https://gitlab.com/hex-m" target="_blank">@hex</a>


##### Author

- <a href="https://gitlab.com/rameardo" target="_blank">Ramear Do</a>

##### License
This project is licensed under the MIT License. This means you can use and modify it for free in private or commercial projects.

